//
//  GameViewController.swift
//  JoustAssignment
//
//  Created by Chandan Swan on 2019-06-20.
//  Copyright © 2019 ekansh sharma. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import AVFoundation

class GameViewController: UIViewController {

    
    // adding sound variable
    var backgroundAudio = AVAudioPlayer()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // sound files
        let sound = Bundle.main.path(forResource: "background", ofType: "mp3")
        
        do {
            backgroundAudio = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
            backgroundAudio.play()
            backgroundAudio.numberOfLoops = -1
            
        }
        catch {
            print(error)
        }
        
       
        
        
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            if let scene = SKScene(fileNamed: "GameScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
